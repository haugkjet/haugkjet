# haugkjet

## 👋 About Me:

- 👨‍💻 I'm a developer from **Stavanger, Norway**
- 🌱 I have a background from the Robotics Industry, recently switched to full-stack web development
- 🌱 I’m currently learning **JavaScript**, **TypeScript** ,**React JS**, **CSS** and **Node.js**
- 👯 I’d be delighted to collaborate with other learners
- **My Personal website** (https://kjetil.xyz)
- **Twitter**, [@haugkjet](https://twitter.com/haugkjet)
- **Github**, [@haugkjet](https://github.com/haugkjet)

